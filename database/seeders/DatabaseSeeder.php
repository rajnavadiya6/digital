<?php

namespace Database\Seeders;

use App\Models\FrontSetting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         \App\Models\User::factory(10)->create();
        $this->call(DefaultUserSeeder::class);
        $this->call(DefaultHomePageRecordSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(FrontSettingSeeder::class);
        $this->call(HomeSliderImageSeeder::class);
        $this->call(AddWhatAppsNumberInSettingSeeder::class);
    }
}
