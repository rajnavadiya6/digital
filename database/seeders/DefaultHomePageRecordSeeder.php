<?php

namespace Database\Seeders;

use App\Models\HomePageSlider;
use Illuminate\Database\Seeder;

class DefaultHomePageRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HomePageSlider::create([
            'name' => 'HomePage'
        ]);
    }
}
