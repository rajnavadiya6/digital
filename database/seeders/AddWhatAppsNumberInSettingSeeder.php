<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class AddWhatAppsNumberInSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create(['key' => 'whatApp_number', 'value' => '+91 70163 33243']);
        Setting::create(['key' => 'whatApp_text', 'value' => 'Hello Navkar Digital!']);
    }
}
