<?php

namespace Database\Seeders;

use App\Models\FrontSetting;
use Illuminate\Database\Seeder;

class FrontSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FrontSetting::create([
            'key'   => 'client_description',
            'page'  => 'home',
            'value' => 'Client satisfaction has always been our first priority because we believe it is an important element for conducting a successful business and yes, we manage to do it. Our potency lies in the dynamic long lasting relationships we develop with our valued clients. We always remember who we are working for YOU! And we mean what we say. Clients always come first for us. They are our driving force. In fact, our persistent focal point of client satisfaction has always resulted in our obsession for providing superior quality work to our clients.',
        ]);
        FrontSetting::create([
            'key'   => 'about_us_description',
            'page'  => 'home',
            'value' => 'Digital Is one of the leading Printing & advertising agencies Rajkot in Gujarat. We have amassed an impressive client list and enjoy enviable reputation amongst our peers as an Navkar Digital of repute.',
        ]);
        FrontSetting::create([
            'key'   => 'map_location',
            'page'  => 'home',
            'value' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59498.46735157954!2d72.84737998591068!3d21.245556435495875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f4fb5c0b087%3A0xb7aabd8a90da0679!2sMota%20Varachha%2C%20Surat%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1612681086821!5m2!1sen!2sin',
        ]);
    }
}
