<?php

namespace Database\Seeders;

use App\Models\HomePageSlider;
use Illuminate\Database\Seeder;

class HomeSliderImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $image = asset('assets/img/blog-inside-post.jpg');

        $homeImage = HomePageSlider::where('id',1)->first();
        $homeImage->update([
           'name' =>  $image,
        ]);

//        $hero1->addMediaFromUrl(asset('assets/img/blog-1.jpg'))->toMediaCollection(HomePageSlider::IMG_PATH);
//        $hero1->addMediaFromUrl(asset('assets/img/blog-2.jpg'))->toMediaCollection(HomePageSlider::IMG_PATH);
//        $hero1->addMediaFromUrl(asset('assets/img/blog-3.jpg'))->toMediaCollection(HomePageSlider::IMG_PATH);
//        $hero1->addMediaFromUrl(asset('assets/img/blog-4.jpg'))->toMediaCollection(HomePageSlider::IMG_PATH);
    }
}
