<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class FrontSetting extends Model
{
    use HasFactory;

    const IMG_PATH = 'home_page';

    public $table = 'front_setting';

    public $fillable = [
        'key',
        'page',
        'value',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'    => 'integer',
        'key'   => 'string',
        'page' => 'string',
        'value' => 'string',
    ];

//    protected $appends = ['image'];

//    protected $with = ['media'];

    /**
     *
     * @return bool
     */
//    public function getImageAttribute()
//    {
//        /** @var Media $media */
//        $media = $this->getMedia(FrontSetting::IMG_PATH)->first();
//        if (! empty($media)) {
//            return $media->getFullUrl();
//        }
//
//        return asset($this->value);
//    }
}

