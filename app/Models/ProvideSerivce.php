<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class ProvideSerivce extends Model implements HasMedia
{
    use HasFactory;
    use HasMediaTrait;

    public const IMG_PATH = 'provide-services';

    public $table = 'provide_services';

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    protected $appends = ['image'];

    /**
     * @return mixed
     */
    public function getImageAttribute()
    {
        /** @var Media $media */
        $media = $this->getMedia(ProvideSerivce::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset($this->value);
    }
}
