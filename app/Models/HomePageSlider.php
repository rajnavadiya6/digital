<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class HomePageSlider extends Model implements HasMedia
{
    use HasFactory;
    use HasMediaTrait;

    public $table = 'home_page_slider';

    protected $fillable = [
        'name',
    ];

    const IMG_PATH = 'upload/banner_image';
//    const IMG_PATH = 'banner_image';

    protected $appends = ['image'];

//    protected $with = ['media'];

    /**
     * @return mixed
     */
    public function getImageAttribute(): string
    {
        /** @var Media $media */
        $media = $this->getMedia(self::IMG_PATH)->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset('assets/img/slide/slide-1.jpg');
    }
}
