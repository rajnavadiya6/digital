<?php

namespace App\DataTables;

use App\Models\ContactUs;

class ContactUsDataTable
{
    /**
     * @return ContactUs
     */
    public function get()
    {
        /** @var ContactUs $query */
        $query = ContactUs::query()->orderByDesc('updated_at')->select('contact_us.*');

        return $query;
    }
}
