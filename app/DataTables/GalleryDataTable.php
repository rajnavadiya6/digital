<?php

namespace App\DataTables;

use App\Models\Gallery;

class GalleryDataTable
{
    /**
     * @return Gallery
     */
    public function get()
    {
        /** @var Gallery $query */
        $query = Gallery::query()->orderByDesc('updated_at')->select('gallery.*');

        return $query;
    }
}
