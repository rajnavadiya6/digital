<?php

namespace App\Http\Controllers;

use App\DataTables\ProvideServiceDataTable;
use App\Http\Requests\CreateProvideServiceRequest;
use App\Http\Requests\UpdateProvideServiceRequest;
use App\Models\ProvideSerivce;
use App\Repositories\ProvideSerivceRepository;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProvideServiceController extends AppBaseController
{
    /** @var  ProvideSerivceRepository  $provideSerivceRepository */
    private $provideSerivceRepository;

    public function __construct(ProvideSerivceRepository $provideSerivceRepository)
    {
        $this->provideSerivceRepository = $provideSerivceRepository;
    }

    /**
     * Display a listing of the Color.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of((new ProvideServiceDataTable())->get())->make(true);
        }

        return view('provide_services.index');
    }

    /**
     * Store a newly created Color in storage.
     *
     * @param CreateProvideServiceRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateProvideServiceRequest $request)
    {
        $input = $request->all();

        $service = ProvideSerivce::create($input);

        if (isset($input['image']) && ! empty($input['image'])) {
            $service->addMedia($input['image'])->toMediaCollection(ProvideSerivce::IMG_PATH);
        }
//        $input = $request->all();
//
//        $color = $this->provideSerivceRepository->create($input);

        return $this->sendSuccess('Provide Serivce saved successfully.');
    }

    /**
     * Display the specified Color.
     *
     * @param ProvideSerivce $provide_service
     *
     * @return JsonResponse
     */
    public function show(ProvideSerivce $provide_service)
    {
        return $this->sendResponse($provide_service, 'Provide Serivce Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Color.
     *
     * @param ProvideSerivce $provide_service
     *
     * @return JsonResponse
     */
    public function edit(ProvideSerivce $provide_service)
    {
        return $this->sendResponse($provide_service, 'Provide Serivce Retrieved Successfully.');
    }

    /**
     * Update the specified Color in storage.
     *
     * @param UpdateProvideServiceRequest $request
     * @param ProvideSerivce $provide_service
     *
     * @return JsonResponse
     */
    public function update(UpdateProvideServiceRequest $request,ProvideSerivce $provide_service)
    {
        $input = $request->all();
        $service = $this->provideSerivceRepository->update($input, $provide_service->id);

        if (isset($input['image']) && ! empty($input['image'])) {
            $service->clearMediaCollection(ProvideSerivce::IMG_PATH);
            $service->addMedia($input['image'])->toMediaCollection(ProvideSerivce::IMG_PATH);
        }
//        $this->provideSerivceRepository->update($request->all(), $provide_service->id);

        return $this->sendSuccess('Provide Serivce updated successfully.');
    }

    /**
     * Remove the specified Color from storage.
     *
     * @param ProvideSerivce $provide_service
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(ProvideSerivce $provide_service)
    {
        $provide_service->clearMediaCollection(ProvideSerivce::IMG_PATH);
        $provide_service->delete();

        return $this->sendSuccess('Provide Serivce deleted successfully.');
    }
}
