<?php

namespace App\Http\Controllers;

use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use App\Models\HomePageSlider;
use App\Repositories\FrontSettingRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Laracasts\Flash\Flash;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig;

class FrontSettingController extends AppBaseController
{
    /** @var  FrontSettingRepository $frontSettingRepository */
    private $frontSettingRepository;

    public function __construct(FrontSettingRepository $frontSettingRepository)
    {
        $this->frontSettingRepository = $frontSettingRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function home(Request $request)
    {
//        $frontSetting = FrontSetting::pluck('value', 'key')->toArray();
        $heroImage = HomePageSlider::all();
        $frontSetting = FrontSetting::where('page','home')->pluck('value', 'key')->toArray();

        return view("settings.front_settings.home", compact('heroImage','frontSetting'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function homeUpdate(Request $request)
    {
        $this->frontSettingRepository->homeUpdateSetting($request,$request->all());

        Flash::success('Setting updated successfully.');

        return Redirect::back();
    }

    /**
     * @param HomePageSlider $homePageSlider
     * @return mixed
     */
    public function deleteHomeSlider(HomePageSlider $homePageSlider)
    {
        $homePageSlider->delete();

        return $this->sendSuccess('Image Deleted.');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function contactUs(Request $request)
    {
        $frontSetting = FrontSetting::pluck('value', 'key')->toArray();

        return view("settings.front_settings.contact_us", compact('frontSetting'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function contactUsUpdate(Request $request)
    {
        $this->frontSettingRepository->contactUsUpdateSetting($request->all());

        Flash::success('Contact us updated successfully.');

        return Redirect::back();
    }

    /**
     *
     * @return JsonResponse
     */
    public function getHomeHeroImages()
    {
        $heroImage = HomePageSlider::with('media')->first();
        $heroImageArr = [];
        foreach ($heroImage->media as $media) {
            $heroImageArr[$media->id] = $media->getUrl();
        }

        return $this->sendResponse($heroImageArr, 'Images retrieved successfully');
    }
}
