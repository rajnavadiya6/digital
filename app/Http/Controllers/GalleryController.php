<?php

namespace App\Http\Controllers;

use App\DataTables\GalleryDataTable;
use App\Models\Gallery;
use App\Repositories\GalleryRepository;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GalleryController extends AppBaseController
{
    /** @var  GalleryRepository  $galleryRepository */
    private $galleryRepository;

    public function __construct(GalleryRepository $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    /**
     * Display a listing of the Color.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of((new GalleryDataTable())->get())->make(true);
        }

        return view('gallery.index');
    }

    /**
     * Store a newly created Color in storage.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $gallery = Gallery::create($input);

        if (isset($input['image']) && ! empty($input['image'])) {
            $gallery->addMedia($input['image'])->toMediaCollection(Gallery::IMG_PATH);
        }

        return $this->sendSuccess('Gallery saved successfully.');
    }

    /**
     * Display the specified Color.
     *
     * @param Gallery $gallery
     *
     * @return JsonResponse
     */
    public function show(Gallery $gallery)
    {
        return $this->sendResponse($gallery, 'Gallery Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Color.
     *
     * @param Gallery $gallery
     *
     * @return JsonResponse
     */
    public function edit(Gallery $gallery)
    {
        return $this->sendResponse($gallery, 'Gallery Retrieved Successfully.');
    }

    /**
     * Update the specified Color in storage.
     *
     * @param Request $request
     * @param Gallery $gallery
     *
     * @return JsonResponse
     */
    public function update(Request $request,Gallery $gallery)
    {
        $input = $request->all();
        $gallery = $this->galleryRepository->update($input, $gallery->id);

        if (isset($input['image']) && ! empty($input['image'])) {
            $gallery->clearMediaCollection(Gallery::IMG_PATH);
            $gallery->addMedia($input['image'])->toMediaCollection(Gallery::IMG_PATH);
        }

        return $this->sendSuccess('Gallery updated successfully.');
    }

    /**
     * Remove the specified Color from storage.
     *
     * @param Gallery $gallery
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(Gallery $gallery)
    {
        $gallery->clearMediaCollection(Gallery::IMG_PATH);
        $gallery->delete();

        return $this->sendSuccess('Gallery deleted successfully.');
    }
}
