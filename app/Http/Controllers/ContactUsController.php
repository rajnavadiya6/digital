<?php

namespace App\Http\Controllers;

use App\DataTables\ContactUsDataTable;
use App\Http\Requests\CreateContactUsRequest;
use App\Models\ContactUs;
use App\Repositories\ContactUsRepository;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Yajra\DataTables\DataTables;

class ContactUsController extends AppBaseController
{
    /** @var  ContactUsRepository  $contactUsRepository */
    private $contactUsRepository;

    public function __construct(ContactUsRepository $contactUsRepository)
    {
        $this->contactUsRepository = $contactUsRepository;
    }

    /**
     * Display a listing of the Color.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of((new ContactUsDataTable())->get())->make(true);
        }

        return view('contact-us.index');
    }

    /**
     * Store a newly created Color in storage.
     *
     * @param CreateContactUsRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateContactUsRequest $request)
    {
        $input = $request->all();

        ContactUs::create($input);

        Flash::success('Your message has been sent. Thank you!');

        return redirect(route('contactUs'));
//        return url('/contactUs')->with('success','');
    }

    /**
     * Display the specified Color.
     *
     * @param ContactUs $contact_u
     *
     * @return JsonResponse
     */
    public function show(ContactUs $contact_u)
    {
        return $this->sendResponse($contact_u, 'Contact us Retrieved Successfully.');
    }

    public function edit()
    {

    }


    public function update()
    {

    }

    public function destroy()
    {

    }
}
