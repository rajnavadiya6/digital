<?php

namespace App\Repositories;

use App\Models\Gallery;

/**
 * Class GalleryRepository
 * @package App\Repositories
 * @version January 15, 2021, 9:48 am UTC
 */

class GalleryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gallery::class;
    }
}
