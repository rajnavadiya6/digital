<?php

namespace App\Repositories;

use App\Models\FrontSetting;
use App\Models\Setting;
use Illuminate\Support\Arr;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig;

/**
 * Class SettingRepository
 */
class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value',
    ];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Setting::class;
    }


    /**
     * @param $input
     * @return bool
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function updateSetting($input)
    {
        $inputArr = Arr::except($input, ['_token']);
        foreach ($inputArr as $key => $value) {
            /** @var Setting $setting */
            $setting = Setting::where('key', $key)->first();
            if (! $setting) {
                continue;
            }

            if (in_array($key, ['logo']) && ! empty($value)) {
                $setting->clearMediaCollection(Setting::IMG_PATH);
                $media = $setting->addMedia($value)
                    ->toMediaCollection(Setting::IMG_PATH);
                $setting->update(['value' => $media->getUrl()]);
            }
            if (in_array($key, ['favicon']) && ! empty($value)) {
                $setting->clearMediaCollection(Setting::IMG_PATH);
                $media = $setting->addMedia($value)
                    ->toMediaCollection(Setting::IMG_PATH);
                $setting->update(['value' => $media->getUrl()]);
            }

            $setting->update(['value' => $value]);
        }

        return true;
    }

    /**
     * @param  Setting  $setting
     * @param $file
     *
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     *
     * @return mixed
     */
    public function fileUpload($setting, $file)
    {
        $setting->clearMediaCollection(Setting::IMG_PATH);
        $media = $setting->addMedia($file)->toMediaCollection(Setting::IMG_PATH);

        $setting->update(['value' => $media->getFullUrl()]);

        return $setting;
    }
}
