<?php

namespace App\Repositories;

use App\Models\ContactUs;

/**
 * Class ContactUsRepository
 * @package App\Repositories
 * @version January 15, 2021, 9:48 am UTC
 */
class ContactUsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name','email','subject','message',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactUs::class;
    }
}
