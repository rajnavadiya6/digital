@extends('settings.index')
@section('title')
    Policy
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update']) }}
{{--    <div class="row mt-3">--}}
{{--        <div class="form-group col-sm-12 my-0">--}}
{{--            {{ Form::label('return_policy', 'Return Policy'.':') }}<span--}}
{{--                class="text-danger">*</span>--}}
{{--            <textarea name="return_policy" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">--}}
{{--               {{ $setting['return_policy'] }}--}}
{{--            </textarea>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row mt-3">--}}
{{--        <div class="form-group col-sm-12 my-0">--}}
{{--            {{ Form::label('privacy_policy', 'Privacy Policy'.':') }}<span--}}
{{--                class="text-danger">*</span>--}}
{{--            <textarea name="privacy_policy" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">--}}
{{--               {{ $setting['privacy_policy'] }}--}}
{{--            </textarea>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row mt-3">--}}
{{--        <div class="form-group col-sm-12 my-0">--}}
{{--            {{ Form::label('offer_policy', 'Offer Policy'.':') }}<span--}}
{{--                class="text-danger">*</span>--}}
{{--            <textarea name="offer_policy" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">--}}
{{--               {{ $setting['offer_policy'] }}--}}
{{--            </textarea>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row mt-3">--}}
{{--        <div class="form-group col-sm-12 my-0">--}}
{{--            {{ Form::label('return_exchange', 'Return Exchange'.':') }}<span--}}
{{--                class="text-danger">*</span>--}}
{{--            <textarea name="return_exchange" class="form-control summerNote" required rows="5" cols="5" style="height: 75% !important;">--}}
{{--               {{ $setting['return_exchange'] }}--}}
{{--            </textarea>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row mt-4">
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
