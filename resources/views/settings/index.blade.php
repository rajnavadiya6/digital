@extends('layouts.app')
@section('title')
    Settings
@endsection
@push('css')
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Settings</h1>
        </div>
        <div class="section-body">
            <div class="card">
                @include('flash::message')
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    @include("settings.setting_menu")
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        'use strict';

        $(document).ready(function (){
            $('.summerNote').summernote({
                height:150,
            });
        });

        $(document).on('change', '#logo', function () {
            // if (isValidFile($(this), '#validationErrorsBox')) {
                displayPhoto(this, '#logoPreview');
            // }
        });

        window.displayFavicon = function (input, selector) {
            let displayPreview = true;
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    let image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        // if ((image.height != 16 || image.width != 16) &&
                        //     (image.height != 32 || image.width != 32)) {
                        //     $('#favicon').val('');
                        //     $('#validationErrorsBox').removeClass('d-none');
                        //     $('#validationErrorsBox').
                        //     html('The image must be of pixel 16 x 16 and 32 x 32.').
                        //     show();
                        //     return false;
                        // }
                        $(selector).attr('src', e.target.result);
                        displayPreview = true;
                    };
                };
                if (displayPreview) {
                    reader.readAsDataURL(input.files[0]);
                    $(selector).show();
                }
            }
        };

        window.isValidFavicon = function (inputSelector, validationMessageSelector) {
            let ext = $(inputSelector).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'ico']) == -1) {
                $(inputSelector).val('');
                $(validationMessageSelector).removeClass('d-none');
                $(validationMessageSelector).
                html('The image must be a file of type: gif, ico, png.').
                show();
                return false;
            }
            $(validationMessageSelector).hide();
            return true;
        };

        $(document).on('change', '#favicon', function () {
            $('#validationErrorsBox').addClass('d-none');
            if (isValidFavicon($(this), '#validationErrorsBox')) {
                displayFavicon(this, '#faviconPreview');
            }
        });
        $(document).on('click', '#btn-reset', function () {
            location.reload();
        });

    </script>
@endpush
