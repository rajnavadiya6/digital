@extends('settings.index')
@section('title')
    WhatsApp
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update', 'files' => 'true', 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) }}
    <div class="row mt-3">
        <div class="form-group col-sm-6">
            {{ Form::label('whatApp_number', 'WhatsApp Number'.':') }}<span
                class="text-danger">*</span>
            <i class="fas fa-question-circle ml-1 mt-1 general-question-mark" data-toggle="tooltip"
               data-placement="top" title="+91 is Required."></i>
            {{ Form::text('whatApp_number', $setting['whatApp_number'], ['class' => 'form-control', 'required']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('whatApp_text', 'WhatsApp Text'.':') }}<span
                class="text-danger">*</span>
            {{ Form::text('whatApp_text', $setting['whatApp_text'], ['class' => 'form-control', 'required']) }}
        </div>
    </div>
    <div class="row mt-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark','id'=>'btn-reset']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
