@extends('settings.index')
@section('title')
    Front Settings
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update','id'=>'editFrontSettingForm']) }}
    <div class="row mt-3">
        <div class="form-group col-sm-12 my-0">
            {{ Form::label('address', 'Address'.':') }}<span
                    class="text-danger">*</span>
            <textarea name="address" class="form-control summerNote" tabindex="0" required rows="5" cols="5" style="height: 75% !important;">
               {{ $setting['address'] }}
            </textarea>
        </div>
        <div class="form-group col-sm-6 mt-3">
            {{ Form::label('phone', 'Phone'.':') }}<span
                    class="text-danger">*</span></br>
            {{ Form::text('phone', $setting['phone'], ['class' => 'form-control','required']) }}
        </div>
        <div class="form-group col-sm-6 mt-3">
            {{ Form::label('email', 'Email'.':') }}<span
                    class="text-danger">*</span>
            {{ Form::email('email', $setting['email'], ['class' => 'form-control', 'required']) }}
        </div>
    </div>

    <div class="row mt-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
@push('scripts')
@endpush
