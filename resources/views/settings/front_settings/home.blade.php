@extends('layouts.app')
@section('title')
    Settings
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/image-uploader.min.css') }}" >
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Home</h1>
        </div>
        <div class="section-body">
            <div class="card">
                @include('flash::message')
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="card-body py-0">
                    {{ Form::open(['route' => 'front.settings.home.update', 'files' => 'true', 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) }}


{{--                    <div class="form-group col-sm-12">--}}
{{--                        <span id="heroImageValidationError" class="text-danger d-none"></span>--}}
{{--                        <span class="text-muted">Recommendation: Best image ratio for Hero image is 1366×768.</span>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-12">--}}
{{--                                {{ Form::label('name', 'Hero Image'.':') }}<span class="text-danger">*</span><br>--}}
{{--                                <div class="input-images"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}





                    <div class="form-group col-xl-12 col-md-12 col-sm-12">
                        {{ Form::label('attachment', 'Home Images'.':') }}<small class="ml-3 text-warning">Recommend Size: (1200 * 500)</small>
                        <div class="d-flex">
                            <label class="image__file-upload bg-primary text-white mr-2"> Choose
                                {{ Form::file('hero_image[]',['id'=>'ticketAttachment','class' => 'd-none','multiple']) }}
                            </label>
                            <div id="attachmentFileSection" class="attachment__create"></div>
                        </div>
                    </div>
                    @if($heroImage)
                        <div class="col-xl-12 col-sm-12 col-lg-12 pl-0">
                            <div class="gallery gallery-md">
                                @foreach($heroImage as $img)
                                    <div class="gallery-item ticket-attachment"
                                         data-image="{{ url($img->name) }}"
                                         data-title="{{ $img->name }}"
                                         href="{{ url($img->name) }}" title="{{ $img->name }}">
                                        <div class="ticket-attachment__icon d-none">
                                            <a href="{{ url($img->name) }}" target="_blank"
                                               class="text-decoration-none text-primary" data-toggle="tooltip"
                                               data-placement="top"
                                               title="View"><i class="fas fa-eye"></i>
                                            </a>
                                            <a href="javascript:void(0)"
                                               class="text-danger text-decoration-none attachment-delete"
                                               data-id="{{ $img->id }}" data-toggle="tooltip" data-placement="top"
                                               title="Delete"><i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('Client Description', 'Client Description'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="client_description" class="form-control summerNote" tabindex="0" required rows="5" cols="5" style="height: 75% !important;">
                               {{ $frontSetting['client_description'] }}
                            </textarea>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('About Us', 'About Us'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="about_us_description" class="form-control summerNote" tabindex="0" required rows="5" cols="5" style="height: 75% !important;">
                               {{ $frontSetting['about_us_description'] }}
                            </textarea>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="form-group col-sm-12 my-0">
                            {{ Form::label('Map', 'Google Map Location Link'.':') }}<span
                                class="text-danger">*</span>
                            <textarea name="map_location" class="form-control" tabindex="0" required rows="5" cols="5" style="height: 75% !important;">
                               {{ $frontSetting['map_location'] }}
                            </textarea>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="form-group col-sm-12">
                            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
    {{--let getHomeHeroImagesURL = "{{ route('get.home.hero.images') }}";--}}

    $(document).ready(function (){
        $('.summerNote').summernote({
            height:150,
        });
    });
    document.querySelector('#ticketAttachment').
    addEventListener('change', handleFileSelect, false);
    let selDiv = document.querySelector('#attachmentFileSection');

    function handleFileSelect (e) {
        if (!e.target.files || !window.FileReader) return;

        selDiv.innerHTML = '';
        let files = e.target.files;
        for (let i = 0; i < files.length; i++) {
            let f = files[i];
            let reader = new FileReader();
            reader.onload = function (e) {
                if (f.type.match('image*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="' +
                        e.target.result + '">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('pdf*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/pdf_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('zip*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/zip_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('sheet*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/xlsx_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('text*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/txt_icon.png">';
                    selDiv.innerHTML += html;
                } else if (f.type.match('msword*')) {
                    let html = '<img class=\'img-thumbnail thumbnail-preview ticket-attachment\' src="/assets/img/doc_icon.png">';
                    selDiv.innerHTML += html;
                } else {
                    selDiv.innerHTML += f.name;
                }

            };
            reader.readAsDataURL(f);
        }
    }

    $(document).on('mouseenter', '.ticket-attachment', function () {
        $(this).find('.ticket-attachment__icon').removeClass('d-none');
    });

    $(document).on('mouseleave', '.ticket-attachment', function () {
        $(this).find('.ticket-attachment__icon').addClass('d-none');
    });

    let deleteMediaUrl = '{{ url('delete-home-slider') }}';

    $(document).on('click', '.attachment-delete', function (event) {
        let id = $(event.currentTarget).attr('data-id');
        swal({
                title: 'Delete !',
                text: 'Are you sure want to delete this "Attachment" ?',
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonColor: '#00b074',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
            },
            function () {
                $.ajax({
                    url: deleteMediaUrl + '/' + id,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (obj) {
                        swal({
                            title: 'Deleted!',
                            text: 'Attachment has been deleted.',
                            type: 'success',
                            confirmButtonColor: '#00b074',
                            timer: 2000,
                        });
                        if (obj.success) {
                            window.location.reload();
                        }

                    },
                    error: function (data) {
                        swal({
                            title: '',
                            text: data.responseJSON.message,
                            type: 'error',
                            confirmButtonColor: '#00b074',
                            timer: 5000,
                        });
                    },
                });
            });
        });


    // if (typeof getHomeHeroImagesURL != 'undefined') {
    //     $(document).ready(function () {
    //         let preloaded = [];
    //         $.ajax({
    //             type: 'GET',
    //             url: getHomeHeroImagesURL,
    //             success: function (result) {
    //                 if (result.data) {
    //                     console.log(typeof (result.data));
    //                     Object.entries(result.data).map((value) => {
    //                         preloaded.push({id: value[0], src: value[1]});
    //                     });
    //                     $('.input-images').imageUploader({
    //                         preloaded: preloaded,
    //                         imagesInputName: 'hero_image',
    //                         preloadedInputName: 'hero_image',
    //                         extensions: ['.jpg', '.jpeg', '.png', '.gif', '.svg'],
    //                     });
    //                 }
    //             },
    //         });
    //     });
    // }
      </script>
@endpush
@section('page_js')
    <script src="{{ asset('assets/js/image-uploader.min.js') }}"></script>
@endsection
