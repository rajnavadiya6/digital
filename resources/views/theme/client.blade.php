@extends('theme.layout.app')
@section('title')
    Clients
@endsection
@push('css')
    <link rel="stylesheet" href="{{ mix('assets/css/web/gallery.css')}}">
@endpush
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container animate__animated animate__bounce">
                <ol>
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li>Client</li>
                </ol>
                <h2>Client</h2>
            </div>
        </section>
        <!-- End Breadcrumbs -->

        <section id="clientSection" class="clientSection">
            <div class="container">
                <div class="row">
                    @foreach($clients as $client)
                        <div class="col-lg-3 mx-3 d-flex justify-content-center " data-aos="{{ $loop->odd ? 'zoom-out-left' : 'zoom-out-right' }}"  style="margin-bottom:60px; padding-left:0px;padding-right:0px;">
                            <div class="icon-box hvr-pulse">
                                <img src="{{ $client->logo_url }}" style="border-radius: 10px;width: 100%;height:100%;">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </main>
@endsection
