@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
@endphp
<!-- ======= Top Bar ======= -->
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex justify-content-between">
        <div class="contact-info">
            <i class="icofont-envelope"></i><a href="mailto:{{ $setting['email'] }}">{{ $setting['email'] }}</a>
            <i class="icofont-phone"></i> {{$setting['phone']}}
        </div>
        <div class="social-links">
            <a href="{{ $setting['twitter_url'] }}" class="twitter"><i class="icofont-twitter"></i>

            </a>
            <a href="{{ $setting['facebook_url'] }}" class="facebook"><i class="icofont-facebook"></i>

            </a>
            <a href="{{ $setting['instagram_url'] }}" class="instagram"><i class="icofont-instagram"></i>

            </a>
            <a href="{{ $setting['pinterest_url'] }}" class="pinterest"><i class="icofont-pinterest"></i>

            </a>
            <a href="{{ $setting['youtube_url'] }}" class="youtube"><i class="icofont-youtube"></i>
            </a>
        </div>
    </div>
</section>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex justify-content-between">

        <div class="logo">
            <h1 class="text-light"><a href="{{ url('/') }}"><span>{{ $setting['application_name'] }}</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="{{ Request::is('/') ? 'active' : '' }}">
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li  class="{{ Request::is('about-us*') ? 'active' : '' }}">
                    <a href="{{ route('about-us') }}">About Us</a>
                </li>
                <li  class="{{ Request::is('our-services*') ? 'active' : '' }}">
                    <a href="{{ route('our-services') }}">Our Services</a>
                </li>
                <li  class="{{ Request::is('galleries*') ? 'active' : '' }}">
                    <a href="{{ route('galleries') }}">Gallery</a>
                </li>
                <li  class="{{ Request::is('client*') ? 'active' : '' }}">
                    <a href="{{ route('client') }}">Our Clients</a>
                </li>
                <li  class="{{ Request::is('contactus*') ? 'active' : '' }}">
                    <a href="{{ route('contactUs') }}">Contact Us</a>
                </li>
            </ul>
        </nav><!-- nav-menu -->

    </div>
</header><!-- End Header -->
