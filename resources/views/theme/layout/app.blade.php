@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
    $setting['logo'] = \App\Models\Setting::whereId(3)->first()->toArray()['logo_url'];
    $setting['favicon'] = \App\Models\Setting::whereId(4)->first()->toArray()['favicon_url'];
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset($setting['favicon']) }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" crossorigin="anonymous" />

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('assets/css/web/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}" />

    @yield('page_css')

    @stack('css')
    <!-- =======================================================
    * Template Name: Eterna - v3.0.0
    * Template URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

    @include('theme.layout.header')

    @yield('content')

    @include('theme.layout.footer')

    <a href="https://api.whatsapp.com/send?phone={{ $setting['whatApp_number'] }}&text={{ $setting['whatApp_text'] }}" target="_blank" class="whatsapp-back-to-top"><i class="icofont-whatsapp"></i></a>

    <a href="#" class="back-to-top">
        <i class="icofont-simple-up"></i>
    </a>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-sticky/jquery.sticky.js')}}"></script>
<script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendor/counterup/counterup.min.js')}}"></script>
<script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js"  crossorigin="anonymous"></script>

<!-- Template Main JS File -->
<script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{ asset('assets/js/aos.js') }}"></script>
    <script>
        AOS.init();
    </script>
    @stack('scripts')

    @yield('page_js')
<script>
    // $('.image-slider').owlCarousel({
    //     margin: 10,
    //     autoplay: true,
    //     loop: true,
    //     autoplayTimeout: 3000,
    //     autoplayHoverPause: true,
    //     responsiveClass: false,
    //     dots: true,
    //     responsive: {
    //         0: {
    //             items: 1,
    //         },
    //         320: {
    //             items: 1,
    //             margin: 20,
    //         },
    //         540: {
    //             items: 1,
    //         },
    //         600: {
    //             items: 1,
    //         },
    //     },
    // });
</script>

</body>

</html>
