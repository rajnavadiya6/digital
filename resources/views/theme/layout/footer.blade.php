@php
    $setting = \App\Models\Setting::pluck('value','key')->toArray();
    $setting['logo'] = \App\Models\Setting::whereId(3)->first()->toArray()['logo_url'];
    $setting['favicon'] = \App\Models\Setting::whereId(4)->first()->toArray()['favicon_url'];
    $frontSetting = \App\Models\FrontSetting::where('page','home')->pluck('value','key')->toArray();
@endphp
<!-- ======= Footer ======= -->
<footer id="footer" data-aos="fade-up">

{{--    <div class="footer-newsletter">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <h4>Our Newsletter</h4>--}}
{{--                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <form action="" method="post">--}}
{{--                        <input type="email" name="email"><input type="submit" value="Subscribe">--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="footer-top" >
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-right">
{{--                    <h4>About Us</h4>--}}
                    <img src="{{ $setting['logo'] }}" style="width: 100%;"/>
{{--                    <h4>Useful Links</h4>--}}
{{--                    <ul>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/') }}">Home</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('about-us') }}">About us</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('our-services') }}">Services</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>--}}
{{--                    </ul>--}}
                </div>

                <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-left">
                    <h4>About Us</h4>
                    <p>{!! $frontSetting['about_us_description'] !!}</p>
{{--                    <ul>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>--}}
{{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>--}}
{{--                    </ul>--}}
                </div>

                <div class="col-lg-3 col-md-6 footer-links footer-contact" data-aos="fade-right">
                    <h4>Quick Link</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('about-us') }}">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('our-services') }}">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('contactUs') }}">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-info" data-aos="fade-right">
                    <h4>Contact Us</h4>
                    <p>
                        {!! $setting['address'] !!}<br>
                        <strong>Phone:</strong> {{ $setting['phone'] }}<br>
                        <strong>Email:</strong> {{ $setting['email'] }}<br>
                    </p>
{{--                    <div class="social-links mt-3">--}}
{{--                        <a href="{{ $setting['twitter_url'] }}" class="twitter"><i class="bx bxl-twitter"></i></a>--}}
{{--                        <a href="{{ $setting['facebook_url'] }}" class="facebook"><i class="bx bxl-facebook"></i></a>--}}
{{--                        <a href="{{ $setting['instagram_url'] }}" class="instagram"><i class="bx bxl-instagram"></i></a>--}}
{{--                        <a href="{{ $setting['pinterest_url'] }}" class="google-plus"><i class="bx bxl-pinterest"></i></a>--}}
{{--                        <a href="{{ $setting['youtube_url'] }}" class="youtube"><i class="bx bxl-youtube"></i></a>--}}
{{--                    </div>--}}
                </div>

            </div>
        </div>
    </div>

    <div class="container" >
        <div class="copyright " data-aos="fade-up"
             data-aos-anchor-placement="top-bottom">
            &copy; Copyright
            <strong>
                <span class="">
                    {{ $setting['application_name'] }}
                </span>
            </strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
        </div>
    </div>
</footer>
<!-- End Footer -->
