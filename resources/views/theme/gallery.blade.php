@extends('theme.layout.app')
@section('title')
    Gallery
@endsection
@push('css')
    <link rel="stylesheet" href="{{ mix('assets/css/web/gallery.css')}}">
@endpush
@section('content')
<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container animate__animated animate__bounce">
            <ol>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Gallery</li>
            </ol>
            <h2 class="animate__animated animate__bounce">Gallery</h2>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <section id="gallerySection" data-aos="fade-up" class="gallerySection">
        <div class="container">
            <div class="row">
                @foreach($galleries as $gallery)
                    <div data-aos="{{ $loop->odd ? 'fade-up-left' : 'fade-up-right' }}"  class=" col-lg-3 mx-3" style="margin-bottom:60px; padding-left:0px;padding-right:0px;">
                        <div class="icon-box hvr-grow-shadow">
                            <img src="{{ $gallery->image_url }}" style="border-radius: 10px;width: 100%;height: 100%;">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
