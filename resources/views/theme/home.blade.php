@extends('theme.layout.app')

@section('content')

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">
                <!-- Slide 1 -->
                @foreach($homeImageSliders as $homeImage)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <img src="{{ $homeImage->name }}">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section><!-- End Hero -->

<main id="main" style="margin-top: 13%">

    <!-- ======= Featured Section ======= -->
{{--    <section id="featured" class="featured">--}}
{{--        <div class="container">--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="icon-box">--}}
{{--                        <i class="icofont-computer"></i>--}}
{{--                        <h3><a href="">Lorem Ipsum</a></h3>--}}
{{--                        <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 mt-4 mt-lg-0">--}}
{{--                    <div class="icon-box">--}}
{{--                        <i class="icofont-image"></i>--}}
{{--                        <h3><a href="">Dolor Sitema</a></h3>--}}
{{--                        <p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 mt-4 mt-lg-0">--}}
{{--                    <div class="icon-box">--}}
{{--                        <i class="icofont-tasks-alt"></i>--}}
{{--                        <h3><a href="">Sed ut perspiciatis</a></h3>--}}
{{--                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </section>--}}
    <!-- End Featured Section -->

    <!-- ======= About Section ======= -->
{{--    <section id="about" class="about">--}}
{{--        <div class="container">--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <img src="{{asset('assets/img/about.jpg')}}" class="img-fluid" alt="">--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 pt-4 pt-lg-0 content">--}}
{{--                    <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>--}}
{{--                    <p class="font-italic">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore--}}
{{--                        magna aliqua.--}}
{{--                    </p>--}}
{{--                    <ul>--}}
{{--                        <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>--}}
{{--                        <li><i class="icofont-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>--}}
{{--                        <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>--}}
{{--                    </ul>--}}
{{--                    <p>--}}
{{--                        Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate--}}
{{--                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in--}}
{{--                        culpa qui officia deserunt mollit anim id est laborum--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </section>--}}
    <!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
        <div class="container">

            <div class="section-title  " data-aos="fade-up">
                <h2 class="animate__animated animate__rubberBand">We Provide Services</h2>
            </div>

            <div class="row">
                @foreach($provideServices as $provideService)
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" 
                     data-aos="fade-right"
                     data-aos-easing="ease-in-sine"
                     style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
                    <i class="fas fa-chevron-right"></i>
                    <span style="margin-left: 12px;margin-top: -5px;">{{ $provideService->name }}</span>
                </div>
                @endforeach
            </div>
            <div class="row w-100 justify-content-center">
                <div class="justify-content-center d-flex mt-4 home-provide-services" data-aos="fade-up">
                    <a class="home-provide-services-btn" href="{{ route('our-services') }}">View More</a>
                </div>
            </div>
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-in">

            <div class="section-title animate__animated animate__bounce">
                <h2>Clients</h2>
                {!! $frontSettings['client_description'] !!}
            </div>

            <div class="owl-carousel clients-carousel">
                @foreach($clients as $client)
                    <img src="{{ $client->logo_url }}" alt="">
                @endforeach
            </div>

        </div>
    </section>
    <!-- End Clients Section -->

</main>
<!-- End #main -->
@endsection
