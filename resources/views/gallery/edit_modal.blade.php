<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Gallery</h5>
                <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
            </div>
            {{ Form::open(['id'=>'editForm', 'files' => true, 'autocomplete' => 'off']) }}
            <div class="modal-body">
                <div class="alert alert-danger d-none" id="editValidationErrorsBox"></div>
                {{ Form::hidden('galleryId', null, ['id' => 'galleryId']) }}
                <div class="row">
                    <div class="form-group col-sm-12">
                        <div class="row">
                            <div class="col-6 col-xl-3">
                                {{ Form::label('image', 'Image'.':') }}
                                <label class="image__file-upload"> Choose
                                    {{ Form::file('image',['id'=>'galleryEditImage','class' => 'd-none']) }}
                                </label>
                            </div>
                            <div class="col-6 col-xl-6 pl-0 mt-1">
                                <img id='galleryEditImagePreview' class="img-thumbnail thumbnail-preview"
                                     src="{{ asset('assets/img/favicon.png') }}">
                                <a href="#" target="_blank" id="imageUrl"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    {{ Form::button('Save', ['type'=>'submit', 'class' => 'btn btn-primary','id' => 'btnEditSave','data-loading-text' => "<span class='spinner-border spinner-border-sm mr-2'></span> Processing..."]) }}
                    <a href="{{ route('gallery.index') }}" class="btn btn-light">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
