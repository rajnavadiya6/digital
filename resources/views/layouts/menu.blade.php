<li class="side-menus {{ Request::is('dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="/">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>
<li class="side-menus {{ Request::is('setting/home*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('front.settings.home') }}">
        <i class="fas fa-home"></i><span>Home Page</span>
    </a>
</li>
<li class="side-menus {{ Request::is('clients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('clients.index') }}">
        <i class=" fas fa-users"></i><span>Clients</span>
    </a>
</li>
<li class="side-menus {{ Request::is('provide-services*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('provide-services.index') }}">
        <i class="fas fa-project-diagram"></i><span>Provide Services</span>
    </a>
</li>
<li class="side-menus {{ Request::is('contact-us*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contact-us.index') }}">
        <i class="fas fa-address-card"></i><span>Contact us</span>
    </a>
</li>
<li class="side-menus {{ Request::is('gallery*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('gallery.index') }}">
        <i class="fas fa-images"></i><span>Gallery</span>
    </a>
</li>
<li class="side-menus {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('settings.index') }}">
        <i class=" fas fa-cog"></i><span>Settings</span>
    </a>
</li>
