@extends('layouts.app')
@section('title')
    Clients
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Clients</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary addClientModal">Client
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
           <div class="card">
                <div class="card-body">
                    @include('clients.table')
                </div>
           </div>
       </div>
    </section>
    @include('clients.add_modal')
    @include('clients.edit_modal')
    @include('clients.show')
@endsection
@push('scripts')
    <script>
        let clientsUrl = "{{ route('clients.index') }}";
        let clientSaveUrl = "{{ route('clients.store') }}";
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{mix('assets/js/clients/clients.js')}}"></script>
@endpush
