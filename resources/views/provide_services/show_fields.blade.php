<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:',['class' => 'font-weight-bold']) !!}
    <p id="show_name"></p>
</div>


<div class="form-group col-md-6">
    {!! Form::label('name', 'Image:',['class' => 'font-weight-bold']) !!}
    <img src="" class="rounded-circle thumbnail-rounded img-bg" id="show_image">
</div>

<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p id="show_created_at"></p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p id="show_updated_at"></p>
</div>


