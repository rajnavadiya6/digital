<div id="addModal" class="modal fade rounded-8 overflow-hidden" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Provide Services</h5>
                <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
            </div>
            {{ Form::open(['id' => 'clients.store','id'=>'createForm', 'files'=>true]) }}
            <div class="modal-body">
                <div class="alert alert-danger d-none" id="validationErrorsBox"></div>
                <div class="row">
                    <div class="form-group col-sm-12">
                        {!! Form::label('name', 'Name:') !!}<span class="text-danger">*</span>
                        {!! Form::text('name', old('name'), ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid':''),'required']) !!}
                        <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                    </div>
                    <div class="form-group col-sm-12">
                        <div class="row">
                            <div class="col-6 col-xl-3">
                                {{ Form::label('app_logo', 'Image'.':') }}
                                <label class="image__file-upload"> Choose
                                    {{ Form::file('image',['id'=>'image','class' => 'd-none']) }}
                                </label>
                            </div>
                            <div class="col-6 col-xl-6 pl-0 mt-1">
                                <img id='imagePreview' class="img-thumbnail thumbnail-preview"
                                     src="{{ asset('assets/img/favicon.png') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    {{ Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'btnSave','data-loading-text'=>"<span class='spinner-border spinner-border-sm mr-2'></span> Processing..."]) }}
                    <a href="{{ route('provide-services.index') }}" class="btn btn-light">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
