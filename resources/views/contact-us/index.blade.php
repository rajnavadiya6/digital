@extends('layouts.app')
@section('title')
    Contact Us
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Contact Us</h1>
        </div>
        <div class="section-body">
           <div class="card">
                <div class="card-body">
                    @include('contact-us.table')
                </div>
           </div>
       </div>
    </section>
    @include('contact-us.show')
@endsection
@push('scripts')
    <script>
        let contactUsUrl = "{{ route('contact-us.index') }}";
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{mix('assets/js/contact_us/contact_us.js')}}"></script>
@endpush
