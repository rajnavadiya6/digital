<table class="table table-responsive-sm table-striped" id="contactUsTbl">
    <thead>
    <tr>
        <th scope="col" class="pl-0  border-0 font-size-4 font-weight-semibold">Name</th>
        <th scope="col" class="pl-0  border-0 font-size-4 font-weight-semibold">Email</th>
        <th scope="col" class="pl-0  border-0 font-size-4 font-weight-semibold">Subject</th>
        <th scope="col" class="pl-0  border-0 font-size-4 font-weight-semibold">Message</th>
        <th scope="col" class="pl-0  border-0 font-size-4 font-weight-semibold">Action</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
