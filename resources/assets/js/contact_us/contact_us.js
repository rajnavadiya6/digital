'use strict';

let tableName = '#contactUsTbl';
$(tableName).DataTable({
    processing: true,
    serverSide: true,
    'order': [[1, 'asc']],
    ajax: {
        url: contactUsUrl,
    },
    columnDefs: [
        {
            'targets': [0],
            'orderable': true,
        },
        {
            'targets': [1],
            'orderable': true,
        },
        {
            'targets': [2],
            'orderable': true,
        },
        {
            'targets': [3],
            'orderable': true,
        },
        {
            'targets': [4],
            'orderable': false,
            'className': 'text-center',
            'width': '165px',
        },
    ],
    columns: [
        {
            data: function (row) {
                return row.name
            },
            name: 'name',
        },
        {
            data: function (row) {
                return row.email
            },
            name: 'email',
        },
        {
            data: function (row) {
                return row.subject
            },
            name: 'subject',
        },
        {
            data: function (row) {
                return row.message
            },
            name: 'message',
        },
        {
            data: function (row) {
                return `<a title="Show" class="btn btn-secondary action-btn show-btn" data-id="${row.id}" href="#">
                            <i class="fa fa-eye"></i>
                        </a>`;
            }, name: 'id',
        },
    ],
});

$(document).on('click', '.show-btn', function (event) {
    let contactUsId = $(event.currentTarget).attr('data-id');
    $.ajax({
        url: contactUsUrl + '/' + contactUsId,
        type: 'GET',
        success: function (result) {
            if (result.success) {
                $('#show_name').html('');
                $('#show_email').html('');
                $('#show_subject').html('');
                $('#show_message').html('');
                $('#show_created_at').html('');
                $('#show_updated_at').html('');

                $('#show_name').append(result.data.name);
                $('#show_email').append(result.data.email);
                $('#show_subject').append(result.data.subject);
                if (!isEmpty(result.data.message)) {
                    let element = document.createElement('textarea');
                    element.innerHTML = result.data.message;
                    $('#show_message').append(element.value);
                } else {
                    $('#show_message').append('N/A');
                }
                let createdDate = moment(result.data.created_at,
                    'YYYY-MM-DD hh:mm:ss').
                format('Do MMM, YYYY hh:mm A');
                $('#show_created_at').append(createdDate);
                let updateDate = moment(result.data.updated_at,
                    'YYYY-MM-DD hh:mm:ss').
                format('Do MMM, YYYY hh:mm A');
                $('#show_updated_at').append(updateDate);
                $('#showModal').appendTo('body').modal('show');
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
});
