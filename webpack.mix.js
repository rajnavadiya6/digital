const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');
    // .
    // postCss('resources/css/app.css', 'public/css', [
    //     require('postcss-import'),
    //     require('tailwindcss'),
    // ]);


mix.styles([
    'public/css/social-icons.css',
    'public/css/owl.carousel.css',
    'public/css/owl.theme.css',
    'public/css/prism.css',
    'public/css/main.css',
    'public/css/custom.css',
], 'public/css/all.css').version();

mix.sass('resources/assets/css/stisla.scss', 'public/assets/css/stisla.css')
    .sass('resources/assets/css/custom.scss', 'public/assets/css/custom.css')
    .sass('resources/assets/css/web/style.scss', 'public/assets/css/web/style.css')
    .sass('resources/assets/css/web/our_services.scss', 'public/assets/css/web/our_services.css')
    .sass('resources/assets/css/web/gallery.scss', 'public/assets/css/web/gallery.css')
    .sass('resources/assets/css/web/client.scss', 'public/assets/css/web/client.css')
    .version();

mix.js('public/js/scripts.js', 'public/js/scripts.min.js')
    .js('resources/assets/js/profile.js', 'public/assets/js/profile.js')
    .js('resources/assets/js/custom/custom.js',
    'public/assets/js/custom/custom.js')
    .js('resources/assets/js/custom/custom-datatable.js',
    'public/assets/js/custom/custom-datatable.js')
    .js('resources/assets/js/custom/common-crud.js',
    'public/assets/js/custom/common-crud.js')
    .js('resources/assets/js/clients/clients.js',
    'public/assets/js/clients/clients.js')
    .js('resources/assets/js/provide_services/provide_services.js',
        'public/assets/js/provide_services/provide_services.js')
    .js('resources/assets/js/contact_us/contact_us.js',
        'public/assets/js/contact_us/contact_us.js')
    .js('resources/assets/js/gallery/gallery.js',
        'public/assets/js/gallery/gallery.js')
.version();


mix.copy('node_modules/sweetalert/dist/sweetalert.css',
    'public/assets/css/sweetalert.css');
mix.copy('node_modules/izitoast/dist/css/iziToast.min.css',
    'public/assets/css/iziToast.min.css');
mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css',
    'public/assets/css/bootstrap.min.css');
mix.copy('node_modules/datatables.net-dt/css/jquery.dataTables.min.css',
    'public/assets/css/jquery.dataTables.min.css');
mix.copy('node_modules/datatables.net-dt/images', 'public/assets/images');

mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/css',
    'public/assets/css/@fortawesome/fontawesome-free/css');
mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts',
    'public/assets/css/@fortawesome/fontawesome-free/webfonts');

mix.babel('node_modules/jquery.nicescroll/dist/jquery.nicescroll.js',
    'public/assets/js/jquery.nicescroll.js');
mix.babel('node_modules/jquery/dist/jquery.min.js',
    'public/assets/js/jquery.min.js');
mix.babel('node_modules/popper.js/dist/umd/popper.min.js',
    'public/assets/js/popper.min.js');
mix.babel('node_modules/bootstrap/dist/js/bootstrap.min.js',
    'public/assets/js/bootstrap.min.js');
mix.babel('node_modules/izitoast/dist/js/iziToast.min.js',
    'public/assets/js/iziToast.min.js');
mix.babel('node_modules/sweetalert/dist/sweetalert.min.js',
    'public/assets/js/sweetalert.min.js');
mix.babel('node_modules/datatables.net/js/jquery.dataTables.min.js',
    'public/assets/js/jquery.dataTables.min.js');
mix.babel('node_modules/moment/min/moment.min.js',
    'public/assets/js/moment.min.js');
