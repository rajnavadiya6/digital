<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FrontSettingController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\ProvideServiceController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\GalleryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/',[HomeController::class,'index'])->name('index');
Route::get('our-services',[HomeController::class,'ourService'])->name('our-services');
Route::get('about-us',[HomeController::class,'aboutUs'])->name('about-us');
Route::get('contactus',[HomeController::class,'contactUs'])->name('contactUs');
Route::get('client',[HomeController::class,'client'])->name('client');
Route::get('galleries',[HomeController::class,'gallery'])->name('galleries');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// front settings
Route::get('setting/home', [FrontSettingController::class, 'home'])->name('front.settings.home');
Route::post('setting/home', [FrontSettingController::class, 'homeUpdate'])->name('front.settings.home.update');
Route::delete('delete-home-slider/{homePageSlider}', [FrontSettingController::class, 'deleteHomeSlider'])->name('front.settings.deleteHomeSlider');
//Route::get('get-home-hero-images',[FrontSettingController::class, 'getHomeHeroImages'])->name('get.home.hero.images');


// settings routes
Route::get('settings', [SettingController::class,'index'])->name('settings.index');
Route::post('settings', [SettingController::class,'update'])->name('settings.update');


Route::resource('provide-services', ProvideServiceController::class);
Route::post('provide-services/{provide_service}/update', [ProvideServiceController::class,'update']);

Route::resource('clients', ClientController::class);
Route::post('clients/{client}/update', [ClientController::class,'update']);

Route::resource('contact-us', ContactUsController::class);

Route::resource('gallery', GalleryController::class);
Route::post('gallery/{gallery}/update', [GalleryController::class,'update']);
