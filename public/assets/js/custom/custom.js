/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/custom/custom.js":
/*!**********************************************!*\
  !*** ./resources/assets/js/custom/custom.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
 // let jsrender = require('jsrender');

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(document).ajaxComplete(function () {
  // Required for Bootstrap tooltips in DataTables
  $('[data-toggle="tooltip"]').tooltip({
    'html': true,
    'offset': 10
  });
});
$('input:text:not([readonly="readonly"])').first().focus();
$(function () {
  $('.modal').on('shown.bs.modal', function () {
    $(this).find('input:text').first().focus();
  });
});

window.resetModalForm = function (formId) {
  $(formId)[0].reset();
  $('select.select2Selector').each(function (index, element) {
    var drpSelector = '#' + $(this).attr('id');
    $(drpSelector).val('');
    $(drpSelector).trigger('change');
  });
  resetFormErrors(formId);
};

window.printErrorMessage = function (selector, errorResult) {
  $(selector).show().html('');
  $(selector).text(errorResult.responseJSON.message);
};

window.manageAjaxErrors = function (data) {
  var errorDivId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'editValidationErrorsBox';

  if (data.status == 404) {
    iziToast.error({
      title: 'Error!',
      message: data.responseJSON.message,
      position: 'topRight'
    });
  } else {
    printErrorMessage('#' + errorDivId, data);
  }
};

window.displaySuccessMessage = function (message) {
  iziToast.success({
    title: 'Success',
    message: message,
    position: 'topRight'
  });
};

window.displayErrorMessage = function (message) {
  iziToast.error({
    title: 'Error',
    message: message,
    position: 'topRight'
  });
};

window.deleteItem = function (url, tableId, header) {
  var callFunction = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  swal({
    title: 'Delete !',
    text: 'Are you sure want to delete this "' + header + '" ?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonColor: '#6777ef',
    cancelButtonColor: '#d33',
    cancelButtonText: 'No',
    confirmButtonText: 'Yes'
  }, function () {
    deleteItemAjax(url, tableId, header, callFunction = null);
  });
};

window.deleteItemAjax = function (url, tableId, header) {
  var callFunction = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  $.ajax({
    url: url,
    type: 'DELETE',
    dataType: 'json',
    success: function success(obj) {
      if (obj.success) {
        if ($(tableId).DataTable().data().count() == 1) {
          $(tableId).DataTable().page('previous').draw('page');
        } else {
          $(tableId).DataTable().ajax.reload(null, false);
        }
      }

      swal({
        title: 'Deleted!',
        text: header + ' has been deleted.',
        type: 'success',
        confirmButtonColor: '#6777ef',
        timer: 2000
      });

      if (callFunction) {
        eval(callFunction);
      }
    },
    error: function error(data) {
      swal({
        title: '',
        text: data.responseJSON.message,
        type: 'error',
        confirmButtonColor: '#6777ef',
        timer: 5000
      });
    }
  });
};

window.deleteLivewireItem = function (url, header) {
  swal({
    title: 'Delete !',
    text: 'Are you sure want to delete this "' + header + '" ?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonColor: '#6777ef',
    cancelButtonColor: '#d33',
    cancelButtonText: 'No',
    confirmButtonText: 'Yes'
  }, function () {
    deleteLivewireItemAjax(url, header);
  });
};

window.deleteLivewireItemAjax = function (url, header) {
  $.ajax({
    url: url,
    type: 'DELETE',
    dataType: 'json',
    success: function success(obj) {
      if (obj.success) {
        window.livewire.emit('refreshComponent');
      }

      swal({
        title: 'Deleted!',
        text: header + ' has been deleted.',
        type: 'success',
        confirmButtonColor: '#6777ef',
        timer: 2000
      });
    },
    error: function error(data) {
      swal({
        title: '',
        text: data.responseJSON.message,
        type: 'error',
        confirmButtonColor: '#6777ef',
        timer: 5000
      });
    }
  });
};

window.format = function (dateTime) {
  var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'DD-MMM-YYYY';
  return moment(dateTime).format(format);
};

window.processingBtn = function (selecter, btnId) {
  var state = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var loadingButton = $(selecter).find(btnId);

  if (state === 'loading') {
    loadingButton.button('loading');
  } else {
    loadingButton.button('reset');
  }
};

window.prepareTemplateRender = function (templateSelector, data) {
  var template = jsrender.templates(templateSelector);
  return template.render(data);
};

window.isValidFile = function (inputSelector, validationMessageSelector) {
  var ext = $(inputSelector).val().split('.').pop().toLowerCase();

  if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'svg', 'ico', 'wbp']) == -1) {
    $(inputSelector).val('');
    $(validationMessageSelector).removeClass('d-none');
    $(validationMessageSelector).html('The image must be a file of type: jpeg, jpg, png.').show();
    return false;
  }

  $(validationMessageSelector).hide();
  return true;
};

window.displayPhoto = function (input, selector) {
  var displayPreview = true;

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var image = new Image();
      image.src = e.target.result;

      image.onload = function () {
        $(selector).attr('src', e.target.result);
        displayPreview = true;
      };
    };

    if (displayPreview) {
      reader.readAsDataURL(input.files[0]);
      $(selector).show();
    }
  }
};

window.removeCommas = function (str) {
  return str.replace(/,/g, '');
};

window.DatetimepickerDefaults = function (opts) {
  return $.extend({}, {
    sideBySide: true,
    ignoreReadonly: true,
    icons: {
      close: 'fa fa-times',
      time: 'fa fa-clock-o',
      date: 'fa fa-calendar',
      up: 'fa fa-arrow-up',
      down: 'fa fa-arrow-down',
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-clock-o',
      clear: 'fa fa-trash-o'
    }
  }, opts);
};

window.isEmpty = function (value) {
  return value === undefined || value === null || value === '';
};

window.screenLock = function () {
  $('#overlay-screen-lock').show();
  $('body').css({
    'pointer-events': 'none',
    'opacity': '0.6'
  });
};

window.screenUnLock = function () {
  $('body').css({
    'pointer-events': 'auto',
    'opacity': '1'
  });
  $('#overlay-screen-lock').hide();
};

window.onload = function () {
  window.startLoader = function () {
    $('.infy-loader').show();
  };

  window.stopLoader = function () {
    $('.infy-loader').hide();
  }; // infy loader js


  stopLoader();
};

$(document).ready(function () {
  // script to active parent menu if sub menu has currently active
  var hasActiveMenu = $(document).find('.nav-item.dropdown ul li').hasClass('active');

  if (hasActiveMenu) {
    $(document).find('.nav-item.dropdown ul li.active').parent('ul').css('display', 'block');
    $(document).find('.nav-item.dropdown ul li.active').parent('ul').parent('li').addClass('active');
  }
});

window.urlValidation = function (value, regex) {
  var urlCheck = value == '' ? true : value.match(regex) ? true : false;

  if (!urlCheck) {
    return false;
  }

  return true;
};

$('.languageSelection').on('click', function () {
  var languageName = $(this).data('prefix-value');
  $.ajax({
    type: 'POST',
    url: '/change-language',
    data: {
      languageName: languageName
    },
    success: function success() {
      location.reload();
    }
  });
});

window.manageFormAjaxErrors = function (result, selector) {
  var response = JSON.parse(result.responseText);

  if (!isEmpty(response.errors)) {
    $.each(response.errors, function (key, value) {
      var inputSelector = "".concat(selector, " :input[name=").concat(key, "]");
      $(inputSelector).addClass('is-invalid');
      $(inputSelector).next('.invalid-feedback').show();
      $(inputSelector).next('.invalid-feedback').text(value);
    });
  } else {
    displayErrorMessage(result.responseJSON.message);
  }
};

window.resetFormErrors = function (selector) {
  $(selector).find('.is-invalid').removeClass('is-invalid');
  $(selector).find('.invalid-feedback').hide();
};

window.formSubmitSuccess = function (result, modelSelector, tableSelector) {
  displaySuccessMessage(result.message);
  $(modelSelector).modal('hide');
  $(tableSelector).DataTable().ajax.reload(null, false);
};

window.submitForm = function (url, type, data, formId, modelId, tableName) {
  var btnId = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : '#btnSave';
  $.ajax({
    url: url,
    type: type,
    data: data,
    success: function success(result) {
      if (result.success) {
        formSubmitSuccess(result, modelId, tableName);
      }
    },
    error: function error(result) {
      resetFormErrors(formId);
      manageFormAjaxErrors(result, formId);
    },
    complete: function complete() {
      processingBtn(formId, btnId);
    }
  });
};

window.formLivewireSubmitSuccess = function (result, modelSelector) {
  displaySuccessMessage(result.message);
  $(modelSelector).modal('hide');
  window.livewire.emit('refreshComponent');
};

window.submitLivewireForm = function (url, type, data, formId, modelId) {
  var btnId = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '#btnSave';
  $.ajax({
    url: url,
    type: type,
    data: data,
    success: function success(result) {
      if (result.success) {
        formLivewireSubmitSuccess(result, modelId);
      }
    },
    error: function error(result) {
      resetFormErrors(formId);
      manageFormAjaxErrors(result, formId);
    },
    complete: function complete() {
      processingBtn(formId, btnId);
    }
  });
};

window.fillEditDataWithKeys = function (data, formSelector) {
  $.each(data, function (key, val) {
    var selector = "".concat(formSelector, " :input[name=").concat(key, "]");
    $(selector).val(val);

    if ($(selector).hasClass('select2-hidden-accessible')) {
      $(selector).val(val).trigger('change');
    }

    if ($(selector).hasClass('custom_datepicker')) {
      $(selector).trigger('change');
    }
  });
};

window.showModelData = function (data) {
  $.each(data, function (key, val) {
    var selector = "#show_".concat(key);

    if (key === 'created_at' || key === 'updated_at' && val !== '') {
      val = format(val, 'Do MMM YY');
    }

    val = isEmpty(val) ? 'N/A' : val;
    $(selector).text(val);
  });
};

/***/ }),

/***/ 3:
/*!****************************************************!*\
  !*** multi ./resources/assets/js/custom/custom.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\Work\digital\resources\assets\js\custom\custom.js */"./resources/assets/js/custom/custom.js");


/***/ })

/******/ });
