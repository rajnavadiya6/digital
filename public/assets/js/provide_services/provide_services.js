!function(e){var a={};function t(r){if(a[r])return a[r].exports;var o=a[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,t),o.l=!0,o.exports}t.m=e,t.c=a,t.d=function(e,a,r){t.o(e,a)||Object.defineProperty(e,a,{enumerable:!0,get:r})},t.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},t.t=function(e,a){if(1&a&&(e=t(e)),8&a)return e;if(4&a&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(t.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&a&&"string"!=typeof e)for(var o in e)t.d(r,o,function(a){return e[a]}.bind(null,o));return r},t.n=function(e){var a=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(a,"a",a),a},t.o=function(e,a){return Object.prototype.hasOwnProperty.call(e,a)},t.p="/",t(t.s=7)}({7:function(e,a,t){e.exports=t("AySK")},AySK:function(e,a,t){"use strict";$("#provideServicesTbl").DataTable({processing:!0,serverSide:!0,order:[[1,"asc"]],ajax:{url:provideServiceUrl},columnDefs:[{targets:[0],orderable:!1},{targets:[1],orderable:!1,width:"50%"},{targets:[2],orderable:!1,className:"text-center",width:"165px"}],columns:[{data:function(e){return'<img src="'+e.image+'" class="rounded-circle thumbnail-rounded img-bg"</img>'},name:"id"},{data:function(e){return e.name},name:"id"},{data:function(e){return'<a title="Show" class="btn btn-secondary action-btn show-btn" data-id="'.concat(e.id,'" href="#">\n                            <i class="fa fa-eye"></i>\n                        </a>\n                      <a title="Edit" class="btn btn-warning action-btn edit-btn" id="').concat(e.id,'" onclick="renderData(').concat(e.id,')" data-toggle="modal"  data-keyboard="true"><i class="fa fa-edit"></i></a>')+'<a title="Delete" class="btn btn-danger action-btn delete-btn" id="'.concat(e.id,'" onclick="deleteData(').concat(e.id,')"><i class="fa fa-trash"></i></a>')},name:"id"}]}),$(".addProvideServicesModal").click((function(){$("#addModal").appendTo("body").modal("show")})),$(document).on("submit","#createForm",(function(e){e.preventDefault(),processingBtn("#createForm","#btnSave","loading"),$.ajax({url:provideServiceSaveUrl,type:"POST",data:new FormData($(this)[0]),processData:!1,contentType:!1,success:function(e){e.success&&(displaySuccessMessage(e.message),$("#addModal").modal("hide"),$("#provideServicesTbl").DataTable().ajax.reload(null,!1))},error:function(e){displayErrorMessage(e.responseJSON.message)},complete:function(){processingBtn("#createForm","#btnSave")}})})),window.renderData=function(e){$.ajax({url:provideServiceUrl+"/"+e+"/edit",type:"GET",success:function(e){e.success&&($("#provideServicesId").val(e.data.id),$("#editName").val(e.data.name),$("#editImagePreview").attr("src",e.data.image),$("#imageUrl").attr("href",e.data.image),$("#imageUrl").text("view"),$("#editModal").appendTo("body").modal("show"))},error:function(e){displayErrorMessage(e.responseJSON.message)}})},$(document).on("submit","#editForm",(function(e){e.preventDefault(),processingBtn("#editForm","#btnEditSave","loading");var a=$("#provideServicesId").val();$.ajax({url:provideServiceUrl+"/"+a+"/update",type:"POST",data:new FormData($(this)[0]),processData:!1,contentType:!1,success:function(e){e.success&&(displaySuccessMessage(e.message),$("#editModal").modal("hide"),$("#provideServicesTbl").DataTable().ajax.reload(null,!1))},error:function(e){displayErrorMessage(e.responseJSON.message)},complete:function(){processingBtn("#editForm","#btnEditSave")}})})),$(document).on("click",".show-btn",(function(e){var a=$(e.currentTarget).attr("data-id");$.ajax({url:provideServiceUrl+"/"+a,type:"GET",success:function(e){if(e.success){$("#show_created_at").html(""),$("#show_updated_at").html(""),$("#show_name").html(""),$("#show_image").html(""),$("#show_name").append(e.data.name),$("#show_image").attr("src",e.data.image);var a=moment(e.data.created_at,"YYYY-MM-DD hh:mm:ss").format("Do MMM, YYYY hh:mm A");$("#show_created_at").append(a);var t=moment(e.data.updated_at,"YYYY-MM-DD hh:mm:ss").format("Do MMM, YYYY hh:mm A");$("#show_updated_at").append(t),$("#showModal").appendTo("body").modal("show")}},error:function(e){displayErrorMessage(e.responseJSON.message)}})})),window.deleteData=function(e){deleteItem(provideServiceUrl+"/"+e,"#provideServicesTbl","Provide Service")};var r=$("#imagePreview").attr("src");$("#addModal").on("hidden.bs.modal",(function(){$("#imagePreview,#editImagePreview").attr("src",r),resetModalForm("#createForm","#validationErrorsBox")})),$("#editModal").on("hidden.bs.modal",(function(){$("#imagePreview,#editImagePreview").attr("src",r),resetModalForm("#editForm","#editValidationErrorsBox")})),$(document).on("change","#image",(function(){isValidFile($(this),"#validationErrorsBox")?displayPhoto(this,"#imagePreview"):$(this).val("")})),$(document).on("change","#editImage",(function(){isValidFile($(this),"#editValidationErrorsBox")?displayPhoto(this,"#editImagePreview"):$(this).val("")}))}});
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/provide_services/provide_services.js":
/*!******************************************************************!*\
  !*** ./resources/assets/js/provide_services/provide_services.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var tableName = '#provideServicesTbl';
$(tableName).DataTable({
  processing: true,
  serverSide: true,
  'order': [[1, 'asc']],
  ajax: {
    url: provideServiceUrl
  },
  columnDefs: [{
    'targets': [0],
    'orderable': false
  }, {
    'targets': [1],
    'orderable': false,
    'width': '50%'
  }, {
    'targets': [2],
    'orderable': false,
    'className': 'text-center',
    'width': '165px'
  }],
  columns: [{
    data: function data(row) {
      return '<img src="' + row.image + '" class="rounded-circle thumbnail-rounded img-bg"' + '</img>';
    },
    name: 'id'
  }, {
    data: function data(row) {
      return row.name;
    },
    name: 'id'
  }, {
    data: function data(row) {
      return "<a title=\"Show\" class=\"btn btn-secondary action-btn show-btn\" data-id=\"".concat(row.id, "\" href=\"#\">\n                            <i class=\"fa fa-eye\"></i>\n                        </a>\n                      <a title=\"Edit\" class=\"btn btn-warning action-btn edit-btn\" id=\"").concat(row.id, "\" onclick=\"renderData(").concat(row.id, ")\" data-toggle=\"modal\"  data-keyboard=\"true\"><i class=\"fa fa-edit\"></i></a>") + "<a title=\"Delete\" class=\"btn btn-danger action-btn delete-btn\" id=\"".concat(row.id, "\" onclick=\"deleteData(").concat(row.id, ")\"><i class=\"fa fa-trash\"></i></a>");
    },
    name: 'id'
  }]
});
$('.addProvideServicesModal').click(function () {
  $('#addModal').appendTo('body').modal('show');
});
$(document).on('submit', '#createForm', function (e) {
  e.preventDefault();
  processingBtn('#createForm', '#btnSave', 'loading');
  $.ajax({
    url: provideServiceSaveUrl,
    type: 'POST',
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function success(result) {
      if (result.success) {
        displaySuccessMessage(result.message);
        $('#addModal').modal('hide');
        $('#provideServicesTbl').DataTable().ajax.reload(null, false);
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    },
    complete: function complete() {
      processingBtn('#createForm', '#btnSave');
    }
  });
});

window.renderData = function (id) {
  $.ajax({
    url: provideServiceUrl + '/' + id + '/edit',
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        $('#provideServicesId').val(result.data.id);
        $('#editName').val(result.data.name);
        $('#editImagePreview').attr('src', result.data.image);
        $('#imageUrl').attr('href', result.data.image);
        $('#imageUrl').text('view');
        $('#editModal').appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
};

$(document).on('submit', '#editForm', function (event) {
  event.preventDefault();
  processingBtn('#editForm', '#btnEditSave', 'loading');
  var id = $('#provideServicesId').val();
  $.ajax({
    url: provideServiceUrl + '/' + id + '/update',
    type: 'POST',
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function success(result) {
      if (result.success) {
        displaySuccessMessage(result.message);
        $('#editModal').modal('hide');
        $('#provideServicesTbl').DataTable().ajax.reload(null, false);
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    },
    complete: function complete() {
      processingBtn('#editForm', '#btnEditSave');
    }
  });
});
$(document).on('click', '.show-btn', function (event) {
  var provideServiceId = $(event.currentTarget).attr('data-id');
  $.ajax({
    url: provideServiceUrl + '/' + provideServiceId,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        $('#show_created_at').html('');
        $('#show_updated_at').html('');
        $('#show_name').html('');
        $('#show_image').html('');
        $('#show_name').append(result.data.name);
        $('#show_image').attr('src', result.data.image);
        var createdDate = moment(result.data.created_at, 'YYYY-MM-DD hh:mm:ss').format('Do MMM, YYYY hh:mm A');
        $('#show_created_at').append(createdDate);
        var updateDate = moment(result.data.updated_at, 'YYYY-MM-DD hh:mm:ss').format('Do MMM, YYYY hh:mm A');
        $('#show_updated_at').append(updateDate);
        $('#showModal').appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
});

window.deleteData = function (id) {
  deleteItem(provideServiceUrl + '/' + id, '#provideServicesTbl', 'Provide Service');
};

var defaultImage = $('#imagePreview').attr('src');
$('#addModal').on('hidden.bs.modal', function () {
  $('#imagePreview,#editImagePreview').attr('src', defaultImage);
  resetModalForm('#createForm', '#validationErrorsBox');
});
$('#editModal').on('hidden.bs.modal', function () {
  $('#imagePreview,#editImagePreview').attr('src', defaultImage);
  resetModalForm('#editForm', '#editValidationErrorsBox');
});
$(document).on('change', '#image', function () {
  var validFile = isValidFile($(this), '#validationErrorsBox');

  if (validFile) {
    displayPhoto(this, '#imagePreview');
  } else {
    $(this).val('');
  }
});
$(document).on('change', '#editImage', function () {
  var validFile = isValidFile($(this), '#editValidationErrorsBox');

  if (validFile) {
    displayPhoto(this, '#editImagePreview');
  } else {
    $(this).val('');
  }
});

/***/ }),

/***/ 7:
/*!************************************************************************!*\
  !*** multi ./resources/assets/js/provide_services/provide_services.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\Work\digital\resources\assets\js\provide_services\provide_services.js */"./resources/assets/js/provide_services/provide_services.js");


/***/ })

/******/ });
